import re
import datetime
import pandas as pd
import googlemaps
import requests
import json
from datetime import datetime as dt
import sys
from pykakasi import kakasi

#https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/hiragana?APIKEY=
#GLOBAL_DOCOMO_APIKEY = "6a332f56316e32774f416c7973657075746d584f695a707542354f6f52504c725751726648663431442e30"
GLOBAL_DOCOMO_APIKEY  = "2e413471337a393246652e3550676c5862395878695563662e4e345531625564734d5867763278416d5330" 


def ch_replace_zen2han(text): 

    if pd.isnull(text):
        return ""

    text = str(text)
    ZEN = "".join(chr(0xff01 + i) for i in range(94))
    ZEN = ZEN + "￥"
    ZEN = ZEN + "ー"
    ZEN = ZEN + "・"


    HAN = "".join(chr(0x21 + i) for i in range(94))
    HAN = HAN + "¥"
    HAN = HAN + "-"
    HAN = HAN + " "
    
    ZEN2HAN = str.maketrans(ZEN, HAN)
   
    if( text != None ):
        return text.translate(ZEN2HAN)
    else:
        return ""

def translate_nn(text):
    
    if pd.isnull(text):
        return ""
    text = str(text)

    # Delete "地" and "、"
    text = re.sub( '[地、]', "", text)

    if( re.match(r'\d{1,4}番\d{1,4}', text) ):
        text = re.sub( '号', "", text)
        stemp = re.match(r'(\d{1,4})番(\d{1,4})', text).groups()
        return stemp[0] + "-" + stemp[1]
    
    if( re.match(r'\d{1,4}番', text) ):
        stemp = re.match(r'(\d{1,4})番', text).groups()
        return stemp[0]

    #When nn月nn日
    if( re.match(r'\d{1,2}月\d{1,2}日', text) ):
        stemp = re.match(r'(\d{1,2})月(\d{1,2})日', text).groups()
        return stemp[0] + "-" + stemp[1]
    
    #When Jan-33
    if( re.match(r'(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\d{1,2}', text) ):
        month_dict = {"Jan":1,"Feb":2,"Mar":3,"Apr":4,"May":5,"Jun":6,"Jul":7,"Aug":8,"Sep":9,"Oct":10,"Nov":11,"Dec":12}
        stemp = re.match(r'(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(\d{1,2})', text).groups()
        return str(month_dict[stemp[0]]) + "-" +stemp[1]
    
    #When UnixTime
    if( re.match(r'\d{4}\/\d{1,2}\/\d{1,2}', text)):
        date_formated = datetime.datetime.strptime(text, "%Y/%m/%d")
        y = date_formated.year - 1970
        if (y > 0):
            stemp = str(y) + "-" + date_formated.strftime("%m-%d") 
        else:
            stemp = date_formated.strftime("%m-%d") 
        text = stemp
    if( re.match(r'/\d{1,2}月\d{1,2}日/', text)):
        date_formated = datetime.datetime.strptime(text, "%m月%d日")
        y = date_formated.year - 1970
        if (y > 0):
            stemp = str(y) + "-" + date_formated.strftime("%m-%d") 
        else:
            stemp = date_formated.strftime("%m-%d") 
        text = stemp

    return text

def trim_string(text):
    if pd.isnull(text):
        return ""
    text = str(text)

    text = re.sub( r'(^\s+)|(\s+$)', "", text) 
    return text

def no_only_hyphen(text):
    if pd.isnull(text):
        return
    text = str(text)
    text = re.sub( r'^-', "", text)
    text = re.sub( r'^‐', "", text)
    return text
    
def sub_building_name_proof(text):
    text = re.sub( r'　', " ", text) #Replace Zenkaku space by hankaku space
    text = re.sub( r'U�', "", text)
    text = re.sub( r'[,、—。☆�]', "", text)
    text = re.sub( r'手数料', "", text)
    text = re.sub( r'\d%', "", text)
    text = re.sub( r'ℹ', "", text)
    text = re.sub( r'residence', "レジデンス", text)
    text = re.sub( r'RESIDENCE', "レジデンス", text)
    return text


def cnv_house_layout_type(text):
    house_layout_type = {
        "1":"one room",
        "2":"K",
        "3":"DK",
        "4":"LK",
        "5":"LDK",
        "6":"SK",
        "7":"SDK",
        "8":"SLK",
        "9":"SLDK"
    }
    res = house_layout_type[text] if (text in house_layout_type)  else "other"
    return res


def get_data_by_type( components, key_type ):
    res = ""
    for i in range( len(components) ):
        value = components[i]
        if key_type in value['types']:
            res += value['long_name']
    return res


def get_data_by_type_for_premise( components, key_type ):
    res = []
    for i in range( len(components) ):
        value = components[i]
        if key_type in value['types']:
            res.append(value['long_name'] )
    res.reverse()
    return res

#Document: https://dev.smt.docomo.ne.jp/?p=docs.api.page&api_name=language_analysis&p_name=api_4
def docomoHiragana(output_type, sentence):
    if sentence == "" or sentence == "　" or sentence == " ":
        return ""
    endpoint = "https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/hiragana?APIKEY=" + GLOBAL_DOCOMO_APIKEY
    request_id = str(int(dt.now().timestamp() * 1000000))
    item_data = { "request_id":request_id, "sentence":sentence, "output_type":output_type }
    headers={'Content-Type': "application/json"}

    response = requests.post(endpoint, headers=headers, json=item_data)
    response_json = json.loads(response.text)
    print(response_json)
    try:
        return response_json["converted"]
    except KeyError:
        print("Error Sentence : " + sentence)
        print(response_json['requestError']['policyException']['text'])
        if response_json['requestError']['policyException']['text'] == "Access denied for crossing the maximum VC messages":
            sys.exit()
        response = requests.post(endpoint, headers=headers, json=item_data)
        response_json = json.loads(response.text)
        print(response_json)
        return response_json["converted"] 

kakasi1 = kakasi()
kakasi2 = kakasi()
kakasi1.setMode("J", "H")
kakasi2.setMode("K", "H") 
conv1 = kakasi1.getConverter()
conv2 = kakasi2.getConverter()
def pykakasiHiragana( sentence ):
    return conv2.do(conv1.do(sentence))
kakasi3 = kakasi()
kakasi3.setMode("H", "a") 
conv3 = kakasi3.getConverter()
def pykakasiHiragana2romaji( hiragana ):
    return conv3.do( hiragana )


def hiragana2romaji( hiragana):
    hepbum = {
        'あ' : 'a', 'い' : 'i', 'う' : 'u', 'え' : 'e', 'お' : 'o',
        'か' : 'ka', 'き' : 'ki', 'く' : 'ku', 'け' : 'ke', 'こ' : 'ko',
        'さ' : 'sa', 'し' : 'shi', 'す' : 'su', 'せ' : 'se', 'そ' : 'so',
        'た' : 'ta', 'ち' : 'chi', 'つ' : 'tsu', 'て' : 'te', 'と' : 'to',
        'な' : 'na', 'に' : 'ni', 'ぬ' : 'nu', 'ね' : 'ne', 'の' : 'no',
        'は' : 'ha', 'ひ' : 'hi', 'ふ' : 'fu', 'へ' : 'he', 'ほ' : 'ho',
        'ま' : 'ma', 'み' : 'mi', 'む' : 'mu', 'め' : 'me', 'も' : 'mo',
        'や' : 'ya', 'ゆ' : 'yu', 'よ' : 'yo',
        'ら' : 'ra', 'り' : 'ri', 'る' : 'ru', 'れ' : 're', 'ろ' : 'ro',
        'わ' : 'wa', 'ゐ' : 'i', 'ゑ' : 'e', 'を' : 'wo', 'ん' : 'n',
        'が' : 'ga', 'ぎ' : 'gi', 'ぐ' : 'gu', 'げ' : 'ge', 'ご' : 'go',
        'ざ' : 'za', 'じ' : 'ji', 'ず' : 'zu', 'ぜ' : 'ze', 'ぞ' : 'zo',
        'だ' : 'da', 'ぢ' : 'ji', 'づ' : 'zu', 'で' : 'de', 'ど' : 'do',
        'ば' : 'ba', 'び' : 'bi', 'ぶ' : 'bu', 'べ' : 'be', 'ぼ' : 'bo',
        'ぱ' : 'pa', 'ぴ' : 'pi', 'ぷ' : 'pu', 'ぺ' : 'pe', 'ぽ' : 'po',
        'きゃ' : 'kya', 'きゅ' : 'kyu', 'きょ' : 'kyo',
        'しゃ' : 'sha', 'しゅ' : 'shu', 'しょ' : 'sho',
        'ちゃ' : 'cha', 'ちゅ' : 'chu', 'ちょ' : 'cho',
        'にゃ' : 'nya', 'にゅ' : 'nyu', 'にょ' : 'nyo',
        'ひゃ' : 'hya', 'ひゅ' : 'hyu', 'ひょ' : 'hyo',
        'みゃ' : 'mya', 'みゅ' : 'myu', 'みょ' : 'myo',
        'りゃ' : 'rya', 'りゅ' : 'ryu', 'りょ' : 'ryo',
        'ぎゃ' : 'gya', 'ぎゅ' : 'gyu', 'ぎょ' :'gya',
        'じゃ' : 'ja', 'じゅ' : 'ju', 'じょ' : 'jo',
        'びゃ' : 'bya', 'びゅ' : 'byu', 'びょ' : 'byo',
        'ぴゃ' : 'pya', 'ぴゅ' : 'pyu', 'ぴょ' : 'pyo',
        "うぃ": "wi" , "うぇ": "we",
        "くぁ": "kwa",
		"くぃ": "kwi",
		"くぅ": "kwu",
		"くぇ": "kwe",
		"くぉ": "kwo",
		"ぐぁ": "gwa",
		"ぐぃ": "gwi",
		"ぐぅ": "gwu",
		"ぐぇ": "gwe",
		"ぐぉ": "gwo",
        "とぁ": "twa",
		"とぃ": "twi",
		"とぅ": "twu",
		"とぇ": "twe",
		"とぉ": "two",
		"どぁ": "dwa",
		"どぃ": "dwi",
		"どぅ": "dwu",
		"どぇ": "dwe",
		"どぉ": "dwo",

		"ふぁ": "fa",
		"ふぃ": "fi",
		"ふぅ": "fuu",
		"ふぇ": "fe",
		"ふぉ": "fo",

		"っか": "kka",
		"っき": "kki",
		"っく": "kku",
		"っけ": "kke",
		"っこ": "kko",
		"っが": "gga",
		"っぎ": "ggi",
		"っぐ": "ggu",
		"っげ": "gge",
		"っご": "ggo",

		"っさ": "ssa",
		"っし": "sshi",
		"っす": "ssu",
		"っせ": "sse",
		"っそ": "sso",
		"っざ": "zza",
		"っじ": "jji",
		"っず": "zzu",
		"っぜ": "zze",
		"っぞ": "zzo",

		"った": "tta",
		"っち": "tchi",
		"っつ": "ttsu",
		"って": "tte",
		"っと": "tto",
		"っだ": "dda",
		"っぢ": "ddi",
		"っづ": "ddu",
		"っで": "dde",
		"っど": "ddo",

		"っは": "hha",
		"っひ": "hhi",
		"っふ": "ffu",
		"っへ": "hhe",
		"っほ": "hho",
		"っば": "bba",
		"っび": "bbi",
		"っぶ": "bbu",
		"っべ": "bbe",
		"っぼ": "bbo",
		"っぱ": "ppa",
		"っぴ": "ppi",
		"っぷ": "ppu",
		"っぺ": "ppe",
		"っぽ": "ppo",

		"っま": "mma",
		"っみ": "mmi",
		"っむ": "mmu",
		"っめ": "mme",
		"っも": "mmo",

		"っや": "yya",
		"っゆ": "yyu",
		"っよ": "yyo",

		"っら": "rra",
		"っり": "rri",
		"っる": "rru",
		"っれ": "rre",
		"っろ": "rro",

		"っわ": "wwa",
		"っゐ": "wwi",
		"っゑ": "wwe",
		"っを": "wwo",

        'ア' : 'a', 'イ' : 'i', 'ウ' : 'u', 'エ' : 'e', 'オ' : 'o',
        'カ' : 'ka', 'キ' : 'ki', 'ク' : 'ku', 'ケ' : 'ke', 'コ' : 'ko',
        'サ' : 'sa', 'シ' : 'shi', 'ス' : 'su', 'セ' : 'se', 'ソ' : 'so',
        'タ' : 'ta', 'チ' : 'chi', 'ツ' : 'tsu', 'テ' : 'te', 'ト' : 'to',
        'ナ' : 'na', 'ニ' : 'ni', 'ヌ' : 'nu', 'ネ' : 'ne', 'ノ' : 'no',
        'ハ' : 'ha', 'ヒ' : 'hi', 'フ' : 'fu', 'ヘ' : 'he', 'ホ' : 'ho',
        'マ' : 'ma', 'ミ' : 'mi', 'ム' : 'mu', 'メ' : 'me', 'モ' : 'mo',
        'ヤ' : 'ya', 'ユ' : 'yu', 'ヨ' : 'yo',
        'ラ' : 'ra', 'リ' : 'ri', 'ル' : 'ru', 'レ' : 're', 'ロ' : 'ro',
        'ワ' : 'wa', 'ヲ' : 'wo', 'ン' : 'n',
        'ガ' : 'ga', 'ギ' : 'gi', 'グ' : 'gu', 'ゲ' : 'ge', 'ゴ' : 'go',
        'ザ' : 'za', 'ジ' : 'ji', 'ズ' : 'zu', 'ゼ' : 'ze', 'ゾ' : 'zo',
        'ダ' : 'da', 'ヂ' : 'ji', 'ヅ' : 'zu', 'デ' : 'de', 'ド' : 'do',
        'バ' : 'ba', 'ビ' : 'bi', 'ブ' : 'bu', 'ベ' : 'be', 'ボ' : 'bo',
        'パ' : 'pa', 'ピ' : 'pi', 'プ' : 'pu', 'ペ' : 'pe', 'ポ' : 'po',
        'ヴ' : 'vu',
        'キャ' : 'kya', 'キュ' : 'kyu', 'キョ' : 'kyo',
        'シャ' : 'sha', 'シュ' : 'shu', 'ショ' : 'sho',
        'チャ' : 'cha', 'チュ' : 'chu', 'チョ' : 'cho',
        'ニャ' : 'nya', 'ニュ' : 'nyu', 'ニョ' : 'nyo',
        'ヒャ' : 'hya', 'ヒュ' : 'hyu', 'ヒョ' : 'hyo',
        'ミャ' : 'mya', 'ミュ' : 'myu', 'ミョ' : 'myo',
        'リャ' : 'rya', 'リュ' : 'ryu', 'リョ' : 'ryo',
        'ギャ' : 'gya', 'ギュ' : 'gyu', 'ギョ' :'gya',
        'ジャ' : 'ja', 'ジュ' : 'ju', 'ジョ' : 'jo',
        'ビャ' : 'bya', 'ビュ' : 'byu', 'ビョ' : 'byo',
        'ピャ' : 'pya', 'ピュ' : 'pyu', 'ピョ' : 'pyo',
        "ヴぁ": "va",
		"ヴぃ": "vi",
		"ヴぅ": "vu",
		"ヴぇ": "ve",
		"ヴぉ": "vo",
        "でぃ": "dyi",
        "ディ": "dyi",
        "ゔぁ": "vi",
        "ゔぃ": "vi",
        "ゔぅ": "vi",
        "ゔぇ": "vi",
        "ゔぉ": "vi"
        }
    hepbum_keys = hepbum.keys()
    hepbum_keys = sorted(hepbum_keys, key=lambda x: len(x), reverse=True)
    re_kanaroma = re.compile("|".join(map(re.escape, hepbum_keys)))
    re_xtu = re.compile("っ(.)")
    re_xtu_katakana = re.compile("ッ(.)")
    re_ltu = re.compile("っ$")
    re_n = re.compile(r"n(b|p)([aiueo])")
    re_oo = re.compile(r"([aiueo])\1")
    romaji = re_kanaroma.sub(lambda x: hepbum[x.group(0)], hiragana)
    romaji = re_xtu.sub(r"\1\1", romaji)
    romaji = re_xtu_katakana.sub(r"\1\1", romaji)
    romaji = re_ltu.sub(r"", romaji)
    romaji = re_n.sub(r"m\1\2", romaji)
    romaji = re_oo.sub(r"\1", romaji)
    
    return romaji


def cnv_romanization( word ):

    # Zenkaku to Hanakaku (Regarding, alphabets & symbols)
    word = ch_replace_zen2han(word)

    # "-" does not appear in alphabets, so delete
    word = word.replace("-", "")
    
    # Kanji & Katakana -> Hiragana
    #word = docomoHiragana("hiragana", word)
    word = pykakasiHiragana( word )
    # Hirgana -> Romaji
    #str_roma = hiragana2romaji( word )
    str_roma = pykakasiHiragana2romaji( word )
    return str_roma

