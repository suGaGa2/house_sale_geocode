import glob
import pandas as pd
import col_data
import utility
import os
import shutil
import datetime
from datetime import datetime as dt
import re
import googlemaps
import functions
import requests
import sys
 

TARGET_PATH = "../TARGET"
CONVERTED_PATH = "../Converted"
DUPLICATED_PATH = "../Converted/Duplicated"
OUTPUT_PATH = "../Output"

def process_jp(df_jp):
    # Choosing the column　
    col_index = []
    for i in range( len(col_data.house_sale_cols) ):
        col_index.append( col_data.house_sale_cols[i][0] )
    df_jp = df_jp.iloc[:, col_index]

    # Process "所在地名3" 
    # Zenkaku to Hanakaku (Regarding, alphabets & symbols)
    df_jp.loc[:, "所在地名3"]  = df_jp.loc[:, "所在地名3"].map(utility.ch_replace_zen2han)
    # Change form date format to nn-nn
    df_jp.loc[:, "所在地名3"]  = df_jp.loc[:, "所在地名3"].map(utility.translate_nn)
    # Delete unnecessary space
    df_jp.loc[:, "所在地名3"]  = df_jp.loc[:, "所在地名3"].map(utility.trim_string)
    # If the string is only hyphen, delete
    df_jp.loc[:, "所在地名3"]  = df_jp.loc[:, "所在地名3"].map(utility.no_only_hyphen)
   
    # Process "間取タイプ（1）"
    df_jp.loc[:, "間取タイプ（1）"] = df_jp.loc[:, "間取タイプ（1）"].map(utility.cnv_house_layout_type)




    # Add ID for debuging
    id_list = []
    for j in range(len(df_jp)):
        id_list.append(str(int(dt.now().timestamp() * 1000000)))
    df_jp.insert(0, "id", id_list)

    df_jp = df_jp.assign(property_code =  'apartment')


    return df_jp



def process_en(df_en):
    en_column_name = dict()
    for j in range( len(col_data.house_sale_cols) ):
        en_column_name[col_data.house_sale_cols[j][1]] = col_data.house_sale_cols[j][2] 
    df_en = df_en.rename(columns=en_column_name)
    return df_en


def geocode(df_jp, df_en):

    # Create a new column "cnv" in df_jp
    df_jp["cnv"] = ""

    # Geocoding start
    no_char_str_lst = ["" for i in range(len(df_jp))]
    df_en.insert(9, "formatted_address", no_char_str_lst)
    df_en.insert(10, "latitude", no_char_str_lst)
    df_en.insert(11, "longitude", no_char_str_lst)
    df_en.insert(12, "postcode_jp", no_char_str_lst)

    df_jp["緯度"] = ""
    df_jp["経度"] = ""

    for j in range(len(df_jp)):
        cnt = 0
        
        #try three times
        while True:
            try:
                address_str = df_jp["都道府県名"][j] + df_jp["所在地名1"][j] + df_jp["所在地名2"][j] + df_jp["所在地名3"][j] 
                
                url = "https://maps.googleapis.com/maps/api/geocode/json"
                params  = {"address" : address_str, "language":"en", "key":'AIzaSyBE6gOQzI2qhvYV8qx2DaZ-_GrEp8XgEgM' }
                r = requests.get(url, params=params)
                geocode_result = r.json()['results']
                geocode_status = r.json()['status']

                try:
                    lng = geocode_result[0]["geometry"]["location"]["lng"]
                    lat = geocode_result[0]["geometry"]["location"]["lat"]
                    print(lng)
                    print(lat)
                    postal_code = utility.get_data_by_type(geocode_result[0]["address_components"], "postal_code")
                    country = utility.get_data_by_type(geocode_result[0]["address_components"], "country")
                    name_of_prefecture = utility.get_data_by_type(geocode_result[0]["address_components"], "administrative_area_level_1")

                    location_name_1 = utility.get_data_by_type( geocode_result[0]["address_components"], "locality" )
                    if utility.get_data_by_type( geocode_result[0]["address_components"], "sublocality_level_1") != "":
                        location_name_1 = location_name_1 + " " + utility.get_data_by_type( geocode_result[0]["address_components"], "sublocality_level_1")
                    
                    location_name_2 = utility.get_data_by_type( geocode_result[0]["address_components"], "sublocality_level_2") 
                    if utility.get_data_by_type( geocode_result[0]["address_components"], "sublocality_level_3") != "":
                        location_name_2 = location_name_2 + " " + utility.get_data_by_type( geocode_result[0]["address_components"], "sublocality_level_3")

                    location_name_3 = utility.get_data_by_type( geocode_result[0]["address_components"], "sublocality_level_4")
                    premise = utility.get_data_by_type_for_premise( geocode_result[0]["address_components"], "premise")
                    for el in premise:
                        if el.isdecimal() or re.match(r'\d{1,4}-\d{1,4}', el):
                            if location_name_3 != "":
                                location_name_3 = location_name_3 + "-"+ el
                            else:
                                location_name_3 = el
                        else :
                            break
                    if location_name_3 == "" and df_jp["所在地名3"][j] != "" :
                        location_name_3 = df_jp["所在地名3"][j]

                    formatted_address = country + ", " + postal_code + " " + name_of_prefecture + ", " + location_name_1 + ", " + location_name_2
                    if location_name_3 != "":
                        formatted_address = formatted_address + ", " + location_name_3

                    print(postal_code)
                    print(country)
                    print(name_of_prefecture)
                    print(location_name_1)
                    print(location_name_2)
                    print(location_name_3)
                    print(formatted_address)

                    df_en["formatted_address"][j] = formatted_address
                    df_en["latitude"][j] = lat
                    df_en["longitude"][j] = lng
                    df_en["postcode_jp"][j] = postal_code
                    df_en["name_of_prefectures"][j] = name_of_prefecture
                    df_en["location_name_1"][j] = location_name_1
                    df_en["location_name_2"][j] = location_name_2
                    df_en["location_name_3"][j] = location_name_3
                    df_en["wayside_abbreviation_1"][j] = utility.cnv_romanization( df_jp["沿線略称（1）"][j]) 
                    df_en["station_name"][j] = utility.cnv_romanization( df_jp["駅名（1）"][j])
                    df_jp["緯度"][j] = lat
                    df_jp["経度"][j] = lng
                    print(df_en["wayside_abbreviation_1"][j])
                    print(df_en["station_name"][j])

                    print("GEOCODE_STATUS : " + geocode_status)

                    
                except IndexError:
                    df_jp["cnv"][j] = 500 # Failure in geocoding
                    print("NO RESULT, SO SKIPPED")

                print("*******************************")
                break
            except SystemExit:
                sys.exit()
            except KeyboardInterrupt:
                sys.exit()
            except: 
                cnt += 1
                if cnt >= 3:
                    break
                pass
    return df_jp, df_en

def export_csv(df_jp, df_en, foldername, filename):
    df_jp.to_csv(OUTPUT_PATH + "/" + foldername + "/JP_"  + filename, index = None)
    df_en.to_csv(OUTPUT_PATH + "/" + foldername + "/EN_"  + filename, index = None)