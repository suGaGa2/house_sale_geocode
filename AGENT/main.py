import glob
import pandas as pd
import col_data
import utility
import os
import shutil
import re
import functions



TARGET_PATH = "../TARGET"
CONVERTED_PATH = "../Converted"
DUPLICATED_PATH = "../Converted/Duplicated"
OUTPUT_PATH = "../Output"

def main():
    folders = glob.glob(TARGET_PATH + "/*")
    print(folders)
    for folders_index in range(len(folders)):
        foldername = os.path.basename( folders[folders_index] ) 
        try:
            os.mkdir(OUTPUT_PATH + "/" + foldername)
        except FileExistsError:
            pass
        files = glob.glob(folders[folders_index] + "/*.csv")
        for i in range(len(files)):
            filename = os.path.basename( files[i] ) 
            print(filename)

            # Check if the file has already been Converted
            # if ture, send the file to Duplicated file Foloder
            if filename in [ r.split('/')[-1] for r in glob.glob(CONVERTED_PATH + "/*.csv" )] :
                shutil.move(TARGET_PATH + '/' + filename, DUPLICATED_PATH) 
                return
           
            # Process of translation of a file
            df_jp = pd.read_csv(files[i], encoding='cp932', dtype=str)
            
            # Process df_jp 
            df_jp = functions.process_jp(df_jp)
            
            # Create and process df_en 
            df_en = functions.process_en(df_jp.copy())

            # Geocoding
            df_jp, df_en = functions.geocode(df_jp, df_en)
            print("OK")
            # Export to CSV
            functions.export_csv(df_jp, df_en, foldername, filename)

            # Move original file to Converted File folder
            shutil.move(TARGET_PATH + '/' + foldername + '/' + filename,  CONVERTED_PATH) 
    

if __name__ == "__main__":
    main()
