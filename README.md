# House / Sale data

# How to use:

1. Import *folders* (not files) that contains .csv file to "TARGET" folder. 
2. Go to "AGENT" and run `python main.py`  (Assuming python 3 environment) 
3. You can get the file in the "Output" Folder with the name "JP_(original file name).csv" and "EN_(original file name).csv".  (CSV files that was converted once in the past is moved to Duplicated Folder without any processing.) 



# APIs:
1. Google geocoding API
2. docomo API


# Problems
・正常に変換されなかった部分を２回目変換するコード準備．そのためにエラーコード整備.
・google ジオコードがデータを日本語で返してくることがある。
・Outputのファイル，県でまとめた方がいいかも？
・建物名がformatted Addressに含まれていた時に，建物名が日本語で返される．
